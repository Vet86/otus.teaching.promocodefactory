﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IList<T> Data { get; set; }

        public InMemoryRepository(IList<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult((IEnumerable<T>)Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task AddAsync(T item)
        {
            return Task.Run(()=>Data.Add(item));
        }

        public Task<bool> RemoveAsync(Guid id)
        {
            var item = Data.FirstOrDefault(x => x.Id == id);
            return Task.FromResult(item is { } && Data.Remove(item));
        }

        public Task<bool> ExistsAsync(Guid id)
        {
            var exists = Data.Any(x => x.Id == id);
            return Task.FromResult(exists);
        }
    }
}