﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Extensions;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _roleRepository;

        public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> roleRepository)
        {
            _employeeRepository = employeeRepository;
            _roleRepository = roleRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            return employee.ToEmployeeResponse();
        }

        /// <summary>
        /// Создать нового сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<EmployeeResponse>> CreateEmployee(NewEmployeeRequest newEmployeeRequest)
        {
            if (!await CheckRolesAsync(newEmployeeRequest.Roles))
                return BadRequest();

            var employee = new Employee
            {
                Id = Guid.NewGuid(),
                Email = newEmployeeRequest.Email,
                FirstName = newEmployeeRequest.FirstName,
                LastName = newEmployeeRequest.LastName,
                AppliedPromocodesCount = newEmployeeRequest.AppliedPromocodesCount,
                Roles = (await Task.WhenAll(newEmployeeRequest.Roles.Select(async x => await _roleRepository.GetByIdAsync(x)))).ToList()
            };

            await _employeeRepository.AddAsync(employee);
            return employee.ToEmployeeResponse();
        }

        /// <summary>
        /// Обновить сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateEmployee(Guid employeeId, NewEmployeeRequest newEmployeeRequest)
        {
            if (!await CheckRolesAsync(newEmployeeRequest.Roles))
                return BadRequest();

            var employee = await _employeeRepository.GetByIdAsync(employeeId);
            if (employee is null)
                return NotFound();

            employee.Email = newEmployeeRequest.Email;
            employee.FirstName = newEmployeeRequest.FirstName;
            employee.LastName = newEmployeeRequest.LastName;
            employee.Email = newEmployeeRequest.Email;
            employee.AppliedPromocodesCount = newEmployeeRequest.AppliedPromocodesCount;
            employee.Roles = (await Task.WhenAll(newEmployeeRequest.Roles.Select(async x => await _roleRepository.GetByIdAsync(x)))).ToList();
            return Ok();
        }

        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteEmployee(Guid employeeId)
        {
            var removed = await _employeeRepository.RemoveAsync(employeeId);
            if (!removed)
                return NotFound();

            return Ok();
        }

        private async Task<bool> CheckRolesAsync(IEnumerable<Guid> roleIds)
        {
            var tasks = roleIds.Select(x => _roleRepository.ExistsAsync(x));
            var results = await Task.WhenAll(tasks);
            return results.All(x => x);
        }
    }
}